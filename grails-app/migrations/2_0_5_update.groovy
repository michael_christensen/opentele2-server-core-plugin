databaseChangeLog = {
    changeSet(author: "RA (kih-1760)", id: "kih-1760-1") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "links_categories")
            }
        }
        createTable(tableName: "links_categories") {
            column(autoIncrement: "true", name: "id", type: '${id.type}') {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "links_categories_PK")
            }

            column(name: "name", type: '${string128.type}') {
                constraints(nullable: "false")
            }

            column(name: "version", type: '${id.type}') { constraints(nullable: "false") }
            column(name: "created_by", type: '${string.type}')
            column(name: "created_date", type: '${datetime.type}')
            column(name: "modified_by", type: '${string.type}')
            column(name: "modified_date", type: '${datetime.type}')
        }
    }

    changeSet(author: "RA (kih-1760)", id: "kih-1760-2") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "links")
            }
        }

        createTable(tableName: "links") {
            column(autoIncrement: "true", name: "id", type: '${id.type}') {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "links_PK")
            }

            column(name: "title", type: '${string128.type}') {
                constraints(nullable: "false")
            }
            column(name: "url", type: '${string2048.type}') {
                constraints(nullable: "false")
            }

            column(name: "links_category_id", type: '${id.type}') {
                constraints(nullable: "false")
            }

            column(name: "version", type: '${id.type}') { constraints(nullable: "false") }
            column(name: "created_by", type: '${string.type}')
            column(name: "created_date", type: '${datetime.type}')
            column(name: "modified_by", type: '${string.type}')
            column(name: "modified_date", type: '${datetime.type}')
        }

        addForeignKeyConstraint(baseColumnNames: "links_category_id", baseTableName: "links",
                constraintName: "link_link_categories_FK",
                referencedColumnNames: "id", referencedTableName: "links_categories", referencesUniqueColumn: "false")

        createIndex(indexName: "links_category_id_idx", tableName: "links") {
            column(name: "links_category_id")
        }
    }

    changeSet(author: "RA (kih-1760)", id: "kih-1760-3") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "links_categories_patient_groups")
            }
        }

        createTable(tableName: "links_categories_patient_groups") {
            column(name: "links_category_id", type: '${id.type}') {
                constraints(nullable: "false")
            }

            column(name: "patient_group_id", type: '${id.type}') {
                constraints(nullable: "false")
            }
        }

        addForeignKeyConstraint(baseColumnNames: "links_category_id", baseTableName: "links_categories_patient_groups",
                constraintName: "links_categories_patient_groups_link_categories_FK",
                referencedColumnNames: "id", referencedTableName: "links_categories", referencesUniqueColumn: "false")
        addForeignKeyConstraint(baseColumnNames: "patient_group_id", baseTableName: "links_categories_patient_groups",
                constraintName: "links_categories_patient_groups_patient_group_FK",
                referencedColumnNames: "id", referencedTableName: "patient_group", referencesUniqueColumn: "false")

        createIndex(indexName: "links_categories_patient_groups_links_category_id_idx", tableName: "links_categories_patient_groups") {
            column(name: "links_category_id")
        }
        createIndex(indexName: "links_categories_patient_groups_patient_group_id_idx", tableName: "links_categories_patient_groups") {
            column(name: "patient_group_id")
        }
    }

    changeSet(author: "EMH (kih-1296)", id: "kih-1296-1") {
        addColumn(tableName: 'measurement') {
            column(name: 'ua_delay', type: '${integer.type}')
        }
        sql(''' update ${fullSchemaName}measurement set ua_delay = 4 ''' )
    }

}
