package org.opentele.server.model

import org.opentele.server.core.model.AbstractObject
import org.opentele.server.core.model.types.MeasurementTypeName

abstract class Threshold extends AbstractObject {

    // Let each subclass have its own table, just as if we didn't have this superclass
    static mapping = { tablePerHierarchy false }

    MeasurementType type

    String toString() {
        type?.name
    }

    public abstract Threshold duplicate()

    public abstract Map thresholdValuesToMap()

    static Threshold createFromValues(String thresholdType, Map thresholdValues) {
        def typeName = MeasurementTypeName.safeValueOf(thresholdType.toUpperCase())
        def type = MeasurementType.findByName(typeName)
        Threshold threshold
        switch (typeName) {
            case MeasurementTypeName.BLOOD_PRESSURE:
                threshold = new BloodPressureThreshold(thresholdValues)
                break;
            case MeasurementTypeName.URINE:
                threshold = new UrineThreshold().fromMap(thresholdValues)
                break;
            case MeasurementTypeName.URINE_GLUCOSE:
                threshold = new UrineGlucoseThreshold().fromMap(thresholdValues)
                break;
            case MeasurementTypeName.URINE_BLOOD:
                threshold = new UrineBloodThreshold().fromMap(thresholdValues)
                break;
            case MeasurementTypeName.URINE_NITRITE:
                threshold = new UrineNitriteThreshold().fromMap(thresholdValues)
                break;
            case MeasurementTypeName.URINE_LEUKOCYTES:
                threshold = new UrineLeukocytesThreshold().fromMap(thresholdValues)
                break;
            default:
                threshold = new NumericThreshold(thresholdValues)
                break;
        }
        threshold.type = type
        threshold
    }
}
