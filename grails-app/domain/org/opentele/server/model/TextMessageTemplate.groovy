package org.opentele.server.model

import org.opentele.server.core.model.AbstractObject

class TextMessageTemplate extends AbstractObject {

    String name
    String content

    def shortContent() {
        content.size() > 50 ? content.substring(0, 50) : content
    }

    def shortName() {
        name.size() > 20 ? name.substring(0, 20) : name
    }

    static constraints = {
        name nullable: false, blank: false, maxSize: 255
        content nullable: false, blank: false, maxSize: 1024
    }
}
