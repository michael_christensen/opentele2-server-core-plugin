package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class UrineGlucoseMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public UrineGlucoseMeasurementNodeOutput(node) {

        OutputVariable glucoseUrine = new OutputVariable(
                "${node.id}.${MeasurementNode.GLUCOSE_URINE_VAR}",
                DataType.INTEGER.value(), 'glucoseUrine')

        this.outputVariables = [glucoseUrine]
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = 'GlucoseUrineDeviceNode'
        this.cancelVariableName = "${node.id}.${MeasurementNode.GLUCOSE_URINE_VAR}#CANCEL"
        this.severityVariableName = "${node.id}.${MeasurementNode.GLUCOSE_URINE_VAR}#SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
