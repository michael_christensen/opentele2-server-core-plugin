package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.QuestionnaireOutputBuilder
import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.patientquestionnaire.PatientMeasurementNode
import org.opentele.server.model.questionnaire.MeasurementNode

class CTGMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public CTGMeasurementNodeOutput(node) {

        OutputVariable deviceId = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.DEVICE_ID_VAR}",
                DataType.STRING.value(), 'deviceId')

        OutputVariable fhr = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.FHR_VAR}",
                DataType.FLOAT_ARRAY.value(), 'fhr')

        OutputVariable mhr = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.MHR_VAR}",
                DataType.FLOAT_ARRAY.value(), 'mhr')

        OutputVariable qfhr = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.QFHR_VAR}",
                DataType.INTEGER_ARRAY.value(), 'qfhr')

        OutputVariable toco = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.TOCO_VAR}",
                DataType.FLOAT_ARRAY.value(), 'toco')

        OutputVariable signal = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.SIGNAL_VAR}",
                DataType.STRING_ARRAY.value(), 'signal')

        OutputVariable signalToNoise = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.SIGNAL_TO_NOISE_VAR}",
                DataType.INTEGER_ARRAY.value(), 'signalToNoise')

        OutputVariable fetalHeight = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.FETAL_HEIGHT_VAR}",
                DataType.INTEGER_ARRAY.value(), 'fetalHeight')

        OutputVariable voltageStart = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.VOLTAGESTART_VAR}",
                DataType.FLOAT.value(), 'voltageStart')

        OutputVariable voltageEnd = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.VOLTAGEEND_VAR}",
                DataType.FLOAT.value(), 'voltageEnd')

        OutputVariable startTime = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.STARTTIME_VAR}",
                DataType.STRING.value(), 'startTime')

        OutputVariable endTime = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.ENDTIME_VAR}",
                DataType.STRING.value(), 'endTime')

        OutputVariable uaDelay = new OutputVariable(
                "${node.id}.CTG#${MeasurementNode.UADELAY_VAR}",
                DataType.INTEGER.value(), 'uaDelay')

        List variables = [
                deviceId, fhr, mhr, qfhr, toco, signal, signalToNoise, fetalHeight,
                voltageStart, voltageEnd, startTime, endTime, uaDelay
        ]

        def customClosure = { aNode, nodeContents, QuestionnaireOutputBuilder outputBuilder ->
            nodeContents['uaSensitivityHigh'] = aNode.monicaUterineActivitySensitivityHigh
            def shouldIncludeMeasuringTime = outputBuilder
                    .inputVariableNameForMeasurementTime(aNode as PatientMeasurementNode) != null
            if (shouldIncludeMeasuringTime) {
                nodeContents['measuringTime'] = [
                        type: 'name',
                        value: outputBuilder
                                .inputVariableNameForMeasurementTime(aNode as PatientMeasurementNode)
                ]
            }
        }

        this.outputVariables = variables
        this.mapToInputFieldsClosure = null
        this.customClosure = customClosure
        this.nodeName = 'MonicaDeviceNode'
        this.cancelVariableName = "${node.id}.CTG##CANCEL"
        this.severityVariableName = "${node.id}.CTG##SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
