package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class UrineBloodMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public UrineBloodMeasurementNodeOutput(node) {

        OutputVariable bloodUrine = new OutputVariable(
                "${node.id}.${MeasurementNode.BLOOD_URINE_VAR}",
                DataType.INTEGER.value(), 'bloodUrine')

        this.outputVariables = [bloodUrine]
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = 'BloodUrineDeviceNode'
        this.cancelVariableName = "${node.id}.${MeasurementNode.BLOOD_URINE_VAR}#CANCEL"
        this.severityVariableName = "${node.id}.${MeasurementNode.BLOOD_URINE_VAR}#SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
