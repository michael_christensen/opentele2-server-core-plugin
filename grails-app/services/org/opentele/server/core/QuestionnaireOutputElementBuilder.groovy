package org.opentele.server.core

import org.opentele.server.core.model.types.DataType

@Singleton
class QuestionnaireOutputElementBuilder {

    static def textViewElement(Map parameters) {
        def contents = [text: parameters.text]

        if (parameters.header != null) {
            contents['header'] = parameters.header
        }

        [TextViewElement: contents]
    }

    static def helpTextElement(Map parameters) {
        def contents = null

        if (parameters.text) {
            contents = [text: parameters.text]
        }

        if (parameters.imageFile) {
            if (contents) {
                contents['imageFile'] = parameters.imageFile
            } else {
                contents = [imageFile: parameters.imageFile]
            }
        }

        [HelpTextElement: contents]
    }

    static def buttonElement(Map parameters) {
        def contents = [
                text: parameters.text,
                gravity: 'center',
                next: parameters.nextNodeId as String
        ]

        if (parameters.skipValidation != null) {
            contents.skipValidation = parameters.skipValidation
        }

        [ButtonElement: contents]
    }

    static def twoButtonElement(Map parameters) {
        [
                TwoButtonElement: [
                        leftText: parameters.leftText,
                        leftNext: parameters.leftNextNodeId,
                        rightText: parameters.rightText,
                        rightNext: parameters.rightNextNodeId
                ]
        ]
    }

    static def radioButtonElement(outputVariables, Map parameters) {
        outputVariables[parameters.variableName as String] = DataType.STRING.value()

        [
                RadioButtonElement: [
                        choices: parameters.choices,
                        outputVariable: [
                                name: parameters.variableName,
                                type: DataType.STRING.value()
                        ]
                ]
        ]
    }

    static def editStringElement(outputVariables, Map parameters) {
        generateEditElement(outputVariables, parameters, DataType.STRING.value())
    }

    static def editIntegerElement(outputVariables, Map parameters) {
        generateEditElement(outputVariables, parameters, DataType.INTEGER.value())
    }

    static def editFloatElement(outputVariables, Map parameters) {
        generateEditElement(outputVariables, parameters, DataType.FLOAT.value())
    }

    static def generateEditElement(outputVariables, Map parameters, String dataType) {
        outputVariables[parameters.variableName as String] = dataType

        [
                EditTextElement: [
                        outputVariable: [
                                name: parameters.variableName,
                                type: dataType
                        ]
                ]
        ]
    }

    static def noteTextElement(outputVariables, parameters) {
        outputVariables[parameters.parent as String] = DataType.STRING.value()

        [
                NoteTextElement: [
                        note: [
                                parent: parameters.parent,
                                type: DataType.STRING.value()
                        ]
                ]
        ]
    }

}
