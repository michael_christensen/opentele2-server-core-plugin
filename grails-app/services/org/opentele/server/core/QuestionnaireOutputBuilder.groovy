package org.opentele.server.core

import org.opentele.server.core.measurement_nodes.*
import org.opentele.server.core.model.patientquestionnaire.PatientQuestionnaireNodeVisitor
import org.opentele.server.model.patientquestionnaire.*
import org.opentele.server.model.questionnaire.MeasurementNode
import org.opentele.server.core.model.types.*
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder

class QuestionnaireOutputBuilder implements PatientQuestionnaireNodeVisitor {

    final Map<String, Closure> meterTypeNameToMeasurementNodeOutput = [:]
    final Map<String, Closure> meterTypeNameToVariableName = [:]

    final List nodes = []
    final Map<String, String> outputVariables = new HashMap<String,String>()
    final MessageSource messageSource

    public QuestionnaireOutputBuilder(MessageSource messageSource) {
        this.messageSource = messageSource
        populateMeterTypeNameToMeasurementNodeOutput()
        populateMeterTypeNameToVariableNameMap()
    }

    def build(PatientQuestionnaire questionnaire) {
        questionnaire.getNodes().each {
            it.visit(this)
        }
    }

    @Override
    void visitEndNode(PatientQuestionnaireNode node) {
        nodes << [
                EndNode: [
                        nodeName: node.id as String
                ]
        ]
    }

    @Override
    void visitBooleanNode(PatientQuestionnaireNode node) {
        addBooleanAssignmentNode(nodeName: "${node.id}",
                nextNodeId: node.defaultNext.id,
                variableName: node.variableName,
                variableValue: node.value)
    }

    @Override
    void visitChoiceNode(PatientQuestionnaireNode node) {
        def severityDefined = node.defaultSeverity || node.alternativeSeverity

        nodes << [
                DecisionNode: [
                        nodeName: node.id as String,
                        // drop assignment nodes if no severity defined
                        next: (severityDefined ?
                                "AN_${node.defaultNext.id}_T${node.id}" :
                                node.defaultNext.id as String),
                        nextFalse: (severityDefined ?
                                "AN_${node.alternativeNext.id}_F${node.id}" :
                                node.alternativeNext.id  as String),
                        expression: patientChoiceNodeExpression(node)
                ]
        ]

        addBooleanAssignmentNode(nodeName: "AN_${node.defaultNext.id}_T${node.id}",
                nextNodeId: "ANSEV_${node.defaultNext.id}_T${node.id}",
                variableName: "${node.id}.DECISION",
                variableValue: true)

        addBooleanAssignmentNode(nodeName: "AN_${node.alternativeNext.id}_F${node.id}",
                nextNodeId: "ANSEV_${node.alternativeNext.id}_F${node.id}",
                variableName: "${node.id}.DECISION",
                variableValue: false)

        // Handle ChoiceNode severity w/assignment nodes..
        if (severityDefined) {
            String defaultValue = node.defaultSeverity?.value() ?: Severity.GREEN.value()
            String alternativeValue = node.alternativeSeverity?.value() ?: Severity.GREEN.value()

            addStringAssignmentNode(nodeName: "ANSEV_${node.defaultNext.id}_T${node.id}",
                    nextNodeId: node.defaultNext.id,
                    variableName: "${node.id}.DECISION#SEVERITY",
                    variableValue: defaultValue)
            addStringAssignmentNode(nodeName: "ANSEV_${node.alternativeNext.id}_F${node.id}",
                    nextNodeId: node.alternativeNext.id,
                    variableName: "${node.id}.DECISION#SEVERITY",
                    variableValue: alternativeValue)
        }
    }

    @Override
    void visitInputNode(PatientQuestionnaireNode node) {
        def severityDefined = node.defaultSeverity || node.alternativeSeverity

        if (node.inputType == DataType.BOOLEAN) {
            String no = messageSource.getMessage('default.yesno.no',
                    new String[0], LocaleContextHolder.locale)
            String yes = messageSource.getMessage('default.yesno.yes',
                    new String[0], LocaleContextHolder.locale)

            if (node.helpInfo) {
                addIoNode(nodeName: node.id, elements: [
                        QuestionnaireOutputElementBuilder.textViewElement(text: node.text),
                        QuestionnaireOutputElementBuilder.helpTextElement(text: node.helpInfo?.text,
                                imageFile: node.helpInfo?.helpImage?.id),
                        QuestionnaireOutputElementBuilder.twoButtonElement(leftText: no,
                                leftNextNodeId: "AN_${node.alternativeNext.id}_L${node.id}",
                                rightText: yes,
                                rightNextNodeId: "AN_${node.defaultNext.id}_R${node.id}")
                ])
            } else {
                addIoNode(nodeName: node.id, elements: [
                        QuestionnaireOutputElementBuilder.textViewElement(text: node.text),
                        QuestionnaireOutputElementBuilder.twoButtonElement(leftText: no,
                                leftNextNodeId: "AN_${node.alternativeNext.id}_L${node.id}",
                                rightText: yes,
                                rightNextNodeId: "AN_${node.defaultNext.id}_R${node.id}")
                ])
            }

            // "Yes" choice
            def yesChoiceNext = severityDefined ?
                    "ANSEV_${node.defaultNext.id}_R${node.id}" :
                    "${node.defaultNext.id}"
            addBooleanAssignmentNode(nodeName: "AN_${node.defaultNext.id}_R${node.id}",
                    nextNodeId: yesChoiceNext,
                    variableName: "${node.id}.FIELD",
                    variableValue: true)

            // "No" choice
            def noChoiceNext = severityDefined ?
                    "ANSEV_${node.alternativeNext.id}_L${node.id}" :
                    node.alternativeNext.id as String
            addBooleanAssignmentNode(nodeName: "AN_${node.alternativeNext.id}_L${node.id}",
                    nextNodeId: noChoiceNext,
                    variableName: "${node.id}.FIELD",
                    variableValue: false)
            // Handle severity w/assignment nodes..

            // Left severity
            if (severityDefined) {
                // "Yes"
                def yesValue = node.defaultSeverity?.value() ?: Severity.GREEN.value()
                addStringAssignmentNode(nodeName: "ANSEV_${node.defaultNext.id}_R${node.id}",
                        nextNodeId: node.defaultNext.id,
                        variableName: "${node.id}.FIELD#SEVERITY", variableValue: yesValue)
                // "No"
                def noValue = node.alternativeSeverity?.value() ?: Severity.GREEN.value()
                addStringAssignmentNode(nodeName: "ANSEV_${node.alternativeNext.id}_L${node.id}",
                        nextNodeId: node.alternativeNext.id,
                        variableName: "${node.id}.FIELD#SEVERITY", variableValue: noValue)
            }
        } else {
            // Patient input node.. but not boolean
            def elements = []

            if (node.inputType == DataType.STRING) {
                elements << QuestionnaireOutputElementBuilder.textViewElement(text: node.text)
                if (node.helpInfo) {
                    elements << QuestionnaireOutputElementBuilder.helpTextElement(text: node.helpInfo?.text,
                            imageFile: node.helpInfo?.helpImage?.id)
                }
                elements << QuestionnaireOutputElementBuilder.editStringElement(outputVariables,
                        [variableName: "${node.id}.FIELD"])
                elements << QuestionnaireOutputElementBuilder.buttonElement(text: messageSource.getMessage('default.choice.next',
                        new String[0], LocaleContextHolder.locale),
                        nextNodeId: node.defaultNext.id)

            } else if (node.inputType == DataType.INTEGER) {
                elements << QuestionnaireOutputElementBuilder.textViewElement(text: node.text)
                if (node.helpInfo) {
                    elements << QuestionnaireOutputElementBuilder.helpTextElement(text: node.helpInfo?.text,
                            imageFile: node.helpInfo?.helpImage?.id)
                }
                elements << QuestionnaireOutputElementBuilder.editIntegerElement(outputVariables,
                        [variableName: "${node.id}.FIELD"])
                elements << QuestionnaireOutputElementBuilder.buttonElement(text: messageSource.getMessage('default.choice.next',
                        new String[0], LocaleContextHolder.locale),
                        nextNodeId: node.defaultNext.id)

            } else if (node.inputType == DataType.FLOAT) {
                elements << QuestionnaireOutputElementBuilder.textViewElement(text: node.text)
                if (node.helpInfo) {
                    elements << QuestionnaireOutputElementBuilder.helpTextElement(text: node.helpInfo?.text,
                            imageFile: node.helpInfo?.helpImage?.id)
                }
                elements << QuestionnaireOutputElementBuilder.editFloatElement(outputVariables,
                        [variableName: "${node.id}.FIELD"])
                elements << QuestionnaireOutputElementBuilder.buttonElement(text: messageSource.getMessage('default.choice.next',
                        new String[0], LocaleContextHolder.locale),
                        nextNodeId: node.defaultNext.id)

            } else if (node.inputType == DataType.RADIOCHOICE) {
                def choices = node.choices.collect { choice(text: it.label, value: it.value) }

                elements << QuestionnaireOutputElementBuilder.textViewElement(text: node.text, header: false)
                if (node.helpInfo) {
                    elements << QuestionnaireOutputElementBuilder.helpTextElement(text: node.helpInfo?.text,
                            imageFile: node.helpInfo?.helpImage?.id)
                }
                elements << QuestionnaireOutputElementBuilder.radioButtonElement(outputVariables,
                        [choices: choices, variableName: "${node.id}.FIELD"])
                elements << QuestionnaireOutputElementBuilder.buttonElement(text: messageSource.getMessage('default.choice.next',
                        new String[0], LocaleContextHolder.locale),
                        nextNodeId: node.defaultNext.id,
                        skipValidation: false)
            } else {
                throw new UnsupportedOperationException("Handling of inputtype: ${node.inputType} is not yet implemented.")
            }

            addIoNode(nodeName: node.id, nextNodeId: node.defaultNext.id, elements: elements)
        }
    }

    @Override
    void visitDelayNode(PatientQuestionnaireNode node) {
        addDelayNode(nodeName: node.id, nextNodeId: node.defaultNext.id,
                elements: [],
                countTime: node.countTime,
                countUp: node.countUp,
                displayTextString: node.text
        )
    }

    @Override
    void visitNoteInputNode(PatientQuestionnaireNode node) {
        addIoNode(nodeName: node.id, nextNodeId: node.defaultNext.id, elements: [
                QuestionnaireOutputElementBuilder.textViewElement(text: node.text),
                QuestionnaireOutputElementBuilder.noteTextElement(outputVariables,
                        [parent: "${node.id}.FIELD"]),
                QuestionnaireOutputElementBuilder.buttonElement(
                        text: messageSource.getMessage('default.choice.next',
                                new String[0], LocaleContextHolder.locale),
                        nextNodeId: node.defaultNext.id)
        ])
    }

    @Override
    void visitTextNode(PatientQuestionnaireNode node) {
        // TextNode maps to simple IONode, with a textfield and a "next" button..
        def elements = []
        if (node.headline != null && node.headline != '') {
            elements << QuestionnaireOutputElementBuilder.textViewElement(text: node.headline)
        }
        elements << QuestionnaireOutputElementBuilder.textViewElement(text: node.text)

        if (node.helpInfo) {
            elements << QuestionnaireOutputElementBuilder.helpTextElement(text: node.helpInfo?.text,
                    imageFile: node.helpInfo?.helpImage?.id)
        }
        elements << QuestionnaireOutputElementBuilder.buttonElement(
                text: messageSource.getMessage('default.choice.next',
                        new String[0], LocaleContextHolder.locale),
                nextNodeId: node.defaultNext.id)

        addIoNode(nodeName: node.id, elements: elements)
    }

    @Override
    void visitMeasurementNode(PatientQuestionnaireNode node) {
        Closure nodeOutputClosure = meterTypeNameToMeasurementNodeOutput[node.meterType.name as String]
        if (nodeOutputClosure) {
            MeasurementNodeOutput nodeOutput = nodeOutputClosure(node)
            patientMeasurementNodeFromNodeOutput(node, nodeOutput)
        } else {
            throw new RuntimeException("Unsupported datatype ${node.meterType.name}")
        }
    }

    private void patientMeasurementNodeFromNodeOutput(node, MeasurementNodeOutput nodeOutput) {

        Closure mapToInputFieldsClosure = nodeOutput.getMapToInputFieldsClosure()
        if (node.mapToInputFields && mapToInputFieldsClosure) {
            mapToInputFieldsClosure(node, this)
        } else {
            def nodeContents = [
                    nodeName: node.id as String,
                    next: defaultNextSeverityNodeName(node) as String,
                    nextFail: cancelNodeName(node),
                    text: node.text,
                    helpText: node.helpInfo?.text,
                    helpImage: node.helpInfo?.helpImage?.id
            ]

            List variables = nodeOutput.getOutputVariables()
            variables.each { OutputVariable variable ->
                outputVariables[variable.name] = variable.type
                nodeContents[variable.key] = [
                        name: variable.name,
                        type: variable.type
                ]
            }

            Closure customClosure = nodeOutput.getCustomClosure()
            if (customClosure) {
                customClosure(node, nodeContents, this)
            }

            def entry = [:]
            entry[nodeOutput.getNodeName()] = nodeContents
            nodes << entry
        }

        addCancelAndSeverityAssignments(node,
                nodeOutput.getCancelVariableName(),
                nodeOutput.getSeverityVariableName())
    }

    private void addCancelAndSeverityAssignments(node, cancelVariableName, severityVariableName) {

        addBooleanAssignmentNode(nodeName: cancelNodeName(node),
                nextNodeId: nextFailSeverityNodeName(node),
                variableName: cancelVariableName,
                variableValue: true)

        def failPathValue = node.nextFailSeverity?.value() ?: Severity.GREEN.value()

        addStringAssignmentNode(nodeName: nextFailSeverityNodeName(node),
                nextNodeId: node.nextFail.id,
                variableName: severityVariableName,
                variableValue: failPathValue)

        def defaultPathValue = node.defaultSeverity?.value() ?: Severity.GREEN.value()

        addStringAssignmentNode(nodeName: defaultNextSeverityNodeName(node),
                nextNodeId: node.defaultNext.id,
                variableName: severityVariableName,
                variableValue: defaultPathValue)
    }

    private def patientChoiceNodeExpression(n) {
        def variableName = inputVariableNameForDecisionNode(n as PatientChoiceNode)
        def options = [
                left: [
                        type:n.dataType.value(),
                        value: n.nodeValue
                ],
                right: [
                        type: 'name',
                        value: variableName
                ]
        ]

        if (n.operation == Operation.GREATER_THAN) {
            [ gt: options ]
        } else if (n.operation == Operation.LESS_THAN) {
            [ lt: options ]
        } else if (n.operation == Operation.EQUALS) {
            [ eq: options ]
        } else {
            throw new RuntimeException("Unsupported operation: ${n.operation}")
        }
    }

    static String cancelNodeName(n) {
        "AN_${n.id}_CANCEL"
    }

    static String defaultNextSeverityNodeName(node) {
        "ANSEV_${node.defaultNext.id}_D${node.id}"
    }

    private static String nextFailSeverityNodeName(node) {
        "ANSEV_${node.nextFail.id}_F${node.id}"
    }

    static String inputVariableNameForMeasurementTime(PatientMeasurementNode dn) {
        if (dn.monicaMeasuringTimeInputNode) {
            "${dn.monicaMeasuringTimeInputNode.id}.${dn.monicaMeasuringTimeInputVar}"
        } else {
            null
        }
    }

    String inputVariableNameForDecisionNode(PatientChoiceNode decisionNode) {
        def inputNode = decisionNode.inputNode
        def inputNodeId = inputNode.id

        if (inputNode.instanceOf(PatientBooleanNode)) {
            decisionNode.inputVar

        } else if (!inputNode.instanceOf(PatientMeasurementNode)) {
            "${inputNodeId}.${decisionNode.inputVar}"

        } else {
            String meterTypeName = inputNode.meterType.name
            Closure variableNameClosure = meterTypeNameToVariableName[meterTypeName]
            if (variableNameClosure) {
                return variableNameClosure(inputNodeId, decisionNode)
            } else {
                return 'ERROR_unsupported'
            }
        }
    }

    void addBooleanAssignmentNode(parameters) {
        outputVariables[parameters.variableName as String] = DataType.BOOLEAN.value()

        nodes << [
                AssignmentNode: [
                        nodeName: parameters.nodeName,
                        next: parameters.nextNodeId as String,
                        variable: [
                                name: parameters.variableName,
                                type: DataType.BOOLEAN.value()
                        ],
                        expression: [
                                type: DataType.BOOLEAN.value(),
                                value: parameters.variableValue
                        ]
                ]
        ]
    }

    void addStringAssignmentNode(parameters) {
        outputVariables[parameters.variableName as String] = DataType.STRING.value()

        nodes << [
                AssignmentNode: [
                        nodeName: parameters.nodeName,
                        next: parameters.nextNodeId as String,
                        variable: [
                                name: parameters.variableName,
                                type: DataType.STRING.value()
                        ],
                        expression: [
                                type: DataType.STRING.value(),
                                value: parameters.variableValue
                        ]
                ]
        ]
    }

    void addIoNode(parameters) {
        def contents = [
                nodeName: parameters.nodeName as String,
                elements: parameters.elements
        ]
        if (parameters.nextNodeId) {
            contents['next'] = parameters.nextNodeId as String
        }

        nodes << [
                IONode: contents
        ]
    }

    void addDelayNode(parameters) {
        def contents = [
                nodeName: parameters.nodeName as String,
                elements: parameters.elements,
                countTime: parameters.countTime,
                countUp: parameters.countUp,
                displayTextString: parameters.displayTextString
        ]
        if (parameters.nextNodeId) {
            contents['next'] = parameters.nextNodeId as String
        }

        nodes << [
                DelayNode: contents
        ]
    }

    def buttonsToSkipInputForNode(parameters) {
        [
                TwoButtonElement: [
                        leftText: messageSource.getMessage('default.choice.omit',
                                new String[0], LocaleContextHolder.locale),
                        leftNext: parameters.leftNext as String,
                        leftSkipValidation: true,

                        rightText: messageSource.getMessage('default.choice.next',
                                new String[0], LocaleContextHolder.locale),
                        rightNext: parameters.rightNext as String,
                        rightSkipValidation: false
                ]
        ]
    }

    static def choice(parameters) {
        [
                value: [
                        type: DataType.STRING.value(),
                        value: parameters.value
                ],
                text: parameters.text
        ]
    }

    Map<String, String> getOutputVariables() {
        return outputVariables
    }

    MessageSource getMessageSource() {
        return messageSource
    }

    // The two maps below are not ideal, but for now it is a better solution than two giant switches

    private void populateMeterTypeNameToMeasurementNodeOutput() {
        if (!meterTypeNameToMeasurementNodeOutput.isEmpty()) { return }

        meterTypeNameToMeasurementNodeOutput[MeterTypeName.BLOOD_PRESSURE_PULSE.value()] = { node ->
            new BloodPressureAndPulseMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.BLOODSUGAR.value()] = { node ->
            new BloodSugarMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.CONTINUOUS_BLOOD_SUGAR_MEASUREMENT.value()] = { node ->
            new ContinuousBloodSugarMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.CRP.value()] = { node ->
            new CRPMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.CTG.value()] = { node ->
            new CTGMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.HEMOGLOBIN.value()] = { node ->
            new HaemoglobinMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.LUNG_FUNCTION.value()] = { node ->
            new LungMonitorMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.SATURATION.value()] = { node ->
            new SaturationMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.SATURATION_W_OUT_PULSE.value()] = { node ->
            new SaturationWithoutPulseMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.TEMPERATURE.value()] = { node ->
            new TemperatureMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.URINE.value()] = { node ->
            new UrineMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.URINE_GLUCOSE.value()] = { node ->
            new UrineGlucoseMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.URINE_BLOOD.value()] = { node ->
            new UrineBloodMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.URINE_NITRITE.value()] = { node ->
            new UrineNitriteMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.URINE_LEUKOCYTES.value()] = { node ->
            new UrineLeukocytesMeasurementNodeOutput(node) }
        meterTypeNameToMeasurementNodeOutput[MeterTypeName.WEIGHT.value()] = { node ->
            new WeightMeasurementNodeOutput(node) }
    }

    private void populateMeterTypeNameToVariableNameMap() {
        if (!meterTypeNameToVariableName.isEmpty()) { return }

        meterTypeNameToVariableName[MeterTypeName.BLOOD_PRESSURE_PULSE.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.BP#${decisionNode.inputVar}" }
        meterTypeNameToVariableName[MeterTypeName.BLOODSUGAR.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.BS#${decisionNode.inputVar}" }
        meterTypeNameToVariableName[MeterTypeName.CRP.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.CRP_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.CTG.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.CTG#${decisionNode.inputVar}" }
        meterTypeNameToVariableName[MeterTypeName.HEMOGLOBIN.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.HEMOGLOBIN_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.SATURATION.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.SAT#${decisionNode.inputVar}" }
        meterTypeNameToVariableName[MeterTypeName.TEMPERATURE.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.TEMPERATURE_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.URINE.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.URINE_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.URINE_BLOOD.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.BLOOD_URINE_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.URINE_GLUCOSE.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.GLUCOSE_URINE_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.URINE_LEUKOCYTES.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.LEUKOCYTES_URINE_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.URINE_NITRITE.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.NITRITE_URINE_VAR}" }
        meterTypeNameToVariableName[MeterTypeName.WEIGHT.value()] = { inputNodeId, decisionNode ->
            "${inputNodeId}.${MeasurementNode.WEIGHT_VAR}" }
    }

}
