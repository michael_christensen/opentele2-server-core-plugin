IF (object_id('links_categories', 'U') is null) BEGIN
  CREATE TABLE [dbo].[links_categories] (
    [id] BIGINT IDENTITY NOT NULL,
    [name] NVARCHAR(128) NOT NULL,
    [version] BIGINT NOT NULL,
    [created_by] NVARCHAR(1024),
    [created_date] datetime2(7),
    [modified_by] NVARCHAR(1024),
    [modified_date] datetime2(7),
    CONSTRAINT [links_categoriesPK] PRIMARY KEY (id)
  );
END
GO

IF (object_id('links', 'U') is null) BEGIN
  CREATE TABLE [dbo].[links] (
    [id] BIGINT IDENTITY NOT NULL,
    [title] NVARCHAR(128) NOT NULL,
    [url] NVARCHAR(2048) NOT NULL,
    [links_category_id] BIGINT NOT NULL,
    [version] BIGINT NOT NULL,
    [created_by] NVARCHAR(1024),
    [created_date] datetime2(7),
    [modified_by] NVARCHAR(1024),
    [modified_date] datetime2(7),
    CONSTRAINT [linksPK] PRIMARY KEY (id),
    CONSTRAINT [link_link_categories_FK] FOREIGN KEY (links_category_id) REFERENCES [dbo].[links_categories] (id)
  );
END
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[links]') AND name = N'links_category_id_idx')
  DROP INDEX [links_category_id_idx] ON [dbo].[links] WITH ( ONLINE = OFF )
GO
CREATE INDEX [links_category_id_idx] ON [dbo].[links]([links_category_id]);
GO

IF (object_id('links_categories_patient_groups', 'U') is null) BEGIN
  CREATE TABLE [dbo].[links_categories_patient_groups] (
    [links_category_id] BIGINT NOT NULL,
    [patient_group_id] BIGINT NOT NULL,
    CONSTRAINT [links_categories_patient_groups_link_categories_FK] FOREIGN KEY ([links_category_id]) REFERENCES [dbo].[links_categories] ([id]),
    CONSTRAINT [links_categories_patient_groups_patient_group_FK] FOREIGN KEY ([patient_group_id]) REFERENCES [dbo].[patient_group] ([id])
  );
END
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[links_categories_patient_groups]') AND name = N'links_categories_patient_groups_links_category_id_idx')
  DROP INDEX [links_categories_patient_groups_links_category_id_idx] ON [dbo].[links_categories_patient_groups] WITH ( ONLINE = OFF )
GO
CREATE INDEX [links_categories_patient_groups_links_category_id_idx] ON [dbo].[links_categories_patient_groups]([links_category_id]);
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[links_categories_patient_groups]') AND name = N'links_categories_patient_groups_patient_group_id_idx')
  DROP INDEX [links_categories_patient_groups_patient_group_id_idx] ON [dbo].[links_categories_patient_groups] WITH ( ONLINE = OFF )
GO
CREATE INDEX [links_categories_patient_groups_patient_group_id_idx] ON [dbo].[links_categories_patient_groups]([patient_group_id]);
GO

ALTER TABLE [dbo].[measurement] ADD [ua_delay] INT
GO
