IF COL_LENGTH('[dbo].[patient]','date_of_birth') IS NULL
BEGIN
  ALTER TABLE [dbo].[patient] ADD [date_of_birth] datetime2(7)
END

IF COL_LENGTH('[dbo].[questionnaire_node]','simulate') IS NOT NULL
BEGIN
  ALTER TABLE [dbo].[questionnaire_node] DROP COLUMN [simulate]
END

IF COL_LENGTH('[dbo].[patient_questionnaire_node]','simulate') IS NOT NULL
BEGIN
  ALTER TABLE [dbo].[patient_questionnaire_node] DROP COLUMN [simulate]
END

IF (object_id('urine_blood_threshold', 'U') is null)
BEGIN
  CREATE TABLE [dbo].[urine_blood_threshold] (
    [id] BIGINT NOT NULL,
    [alert_high] NVARCHAR(1024),
    [alert_low] NVARCHAR(1024),
    [warning_high] NVARCHAR(1024),
    [warning_low] NVARCHAR(1024),
    CONSTRAINT [urine_blood_thresholdPK] PRIMARY KEY ([id]))
END

IF (object_id('urine_nitrite_threshold', 'U') is null)
BEGIN
  CREATE TABLE [dbo].[urine_nitrite_threshold] (
    [id] BIGINT NOT NULL,
    [alert_high] NVARCHAR(1024),
    [alert_low] NVARCHAR(1024),
    [warning_high] NVARCHAR(1024),
    [warning_low] NVARCHAR(1024),
    CONSTRAINT [urine_nitrite_thresholdPK] PRIMARY KEY ([id]))
END

IF (object_id('urine_leukocytes_threshold', 'U') is null)
BEGIN
  CREATE TABLE [dbo].[urine_leukocytes_threshold] (
    [id] BIGINT NOT NULL,
    [alert_high] NVARCHAR(1024),
    [alert_low] NVARCHAR(1024),
    [warning_high] NVARCHAR(1024),
    [warning_low] NVARCHAR(1024),
    CONSTRAINT [urine_leukocytes_thresholdPK] PRIMARY KEY ([id]))
END

IF COL_LENGTH('[dbo].[measurement]','blood_in_urine') IS NULL
BEGIN
  ALTER TABLE [dbo].[measurement] ADD [blood_in_urine] VARCHAR(max)
END

IF COL_LENGTH('[dbo].[measurement]','nitrite_in_urine') IS NULL
BEGIN
  ALTER TABLE [dbo].[measurement] ADD [nitrite_in_urine] VARCHAR(max)
END

IF COL_LENGTH('[dbo].[measurement]','leukocytes_in_urine') IS NULL
BEGIN
  ALTER TABLE [dbo].[measurement] ADD [leukocytes_in_urine] VARCHAR(max)
END

IF COL_LENGTH('[dbo].[conference_measurement_draft]','blood_sugar') IS NULL
BEGIN
  ALTER TABLE [dbo].[conference_measurement_draft] ADD [blood_sugar] double precision
END

IF COL_LENGTH('[dbo].[conference_measurement_draft]','protein') IS NULL
BEGIN
  ALTER TABLE [dbo].[conference_measurement_draft] ADD [protein] VARCHAR(max)
END

IF COL_LENGTH('[dbo].[conference_measurement_draft]','urine_glucose') IS NULL
BEGIN
  ALTER TABLE [dbo].[conference_measurement_draft] ADD [urine_glucose] VARCHAR(max)
END

IF COL_LENGTH('[dbo].[conference_measurement_draft]','urine_blood') IS NULL
BEGIN
  ALTER TABLE [dbo].[conference_measurement_draft] ADD [urine_blood] VARCHAR(max)
END

IF COL_LENGTH('[dbo].[conference_measurement_draft]','urine_nitrite') IS NULL
BEGIN
  ALTER TABLE [dbo].[conference_measurement_draft] ADD [urine_nitrite] VARCHAR(max)
END

IF COL_LENGTH('[dbo].[conference_measurement_draft]','urine_leukocytes') IS NULL
BEGIN
  ALTER TABLE [dbo].[conference_measurement_draft] ADD [urine_leukocytes] VARCHAR(max)
END
