--
-- KIH-1296
--
ALTER TABLE [dbo].[patient_questionnaire_node] ADD [monica_uterine_activity_sensitivity_high] bit
GO
ALTER TABLE [dbo].[questionnaire_node] ADD [monica_uterine_activity_sensitivity_high] bit
GO