package org.opentele.server.core.model.graph

public class Vertex {
    public String name
    public List<Edge> outEdges
    public int inDegree // A convenient *scratch variable* for the hasCycle algorithm to use
    public Vertex(String nm) {
        name = nm
        outEdges = []
        inDegree = 0
    }

    boolean equals(o) {
        if (this.is(o)) {
            return true
        }

        if (getClass() != o.class) {
            return false
        }

        Vertex vertex = (Vertex) o

        if (name != vertex.name) {
            return false
        }

        return true
    }

    int hashCode() {
        return name.hashCode()
    }
}
