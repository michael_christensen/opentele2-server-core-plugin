package org.opentele.server.core.model.types

enum MeterTypeName {

	BLOOD_PRESSURE_PULSE,
    BLOODSUGAR,
    CONTINUOUS_BLOOD_SUGAR_MEASUREMENT,
    CRP,
    CTG,
    HEMOGLOBIN,
    LUNG_FUNCTION,
    SATURATION,
    SATURATION_W_OUT_PULSE,
    TEMPERATURE,
    URINE, //protein in urine
    URINE_BLOOD,
    URINE_COMBI,
    URINE_GLUCOSE,
    URINE_LEUKOCYTES,
    URINE_NITRITE,
    WEIGHT

    String getKey() {
        name()
    }
    
	String value() {
		name()
	}
}
