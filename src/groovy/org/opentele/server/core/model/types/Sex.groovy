package org.opentele.server.core.model.types

enum Sex implements Serializable {

    MALE('M'), FEMALE('F'), UNKNOWN('U')
    
    Sex(String value) { 
        this.value = value 
    }
    
    private final String value

    public static Sex fromString(String value) {
        for(Sex v : values())
            if(v.name().equalsIgnoreCase(value)) return v;
        throw new IllegalArgumentException();
    }

    String value() { 
        value 
    }
}